package ru.alexeyzhulin.currencyrate.exceptions;

public class ApiException extends Exception {
    public ApiException(String message) {
        super(message);
    }
}
