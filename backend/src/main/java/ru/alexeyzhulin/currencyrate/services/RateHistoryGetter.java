package ru.alexeyzhulin.currencyrate.services;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class RateHistoryGetter extends RateGetter {
    private String rateDate;

    protected RateHistoryGetter(Environment environment) {
        super(environment);
    }

    void setRateDate(String rateDate) {
        this.rateDate = rateDate;
    }

    @Override
    String getApiUrl() {
        return "https://fixer-fixer-currency-v1.p.rapidapi.com/" + rateDate + "?base=USD&symbols=RUB";
    }
}
