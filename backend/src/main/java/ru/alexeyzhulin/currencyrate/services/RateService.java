package ru.alexeyzhulin.currencyrate.services;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ru.alexeyzhulin.currencyrate.exceptions.ApiException;
import ru.alexeyzhulin.currencyrate.models.ProfitStructure;

import java.util.Date;

@Component
public class RateService {
    private final RateCurrentGetter rateCurrentGetter;
    private final RateHistoryGetter rateHistoryGetter;
    private final Environment environment;

    public RateService(RateCurrentGetter rateCurrentGetter, RateHistoryGetter rateHistoryGetter, Environment environment) {
        this.rateCurrentGetter = rateCurrentGetter;
        this.rateHistoryGetter = rateHistoryGetter;
        this.environment = environment;
    }

    public ProfitStructure getProfitStructure(String purchaseDate, double amount) throws ApiException, UnirestException {
        rateHistoryGetter.setRateDate(purchaseDate);
        double purchaseRate = rateHistoryGetter.getRateValue();
        double sellRate = rateCurrentGetter.getRateValue();
        double spread = Double.valueOf(environment.getProperty("application.spreadPercent"));
        double profit = sellRate * (1 - spread / 100) * amount -
                purchaseRate * amount;

        return new ProfitStructure(purchaseRate,
                sellRate,
                spread,
                amount,
                profit
                );
    }
}
