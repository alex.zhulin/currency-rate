package ru.alexeyzhulin.currencyrate.services;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class RateCurrentGetter extends RateGetter {

    protected RateCurrentGetter(Environment environment) {
        super(environment);
    }

    @Override
    String getApiUrl() {
        return "https://fixer-fixer-currency-v1.p.rapidapi.com/latest?base=USD&symbols=RUB";
    }
}
