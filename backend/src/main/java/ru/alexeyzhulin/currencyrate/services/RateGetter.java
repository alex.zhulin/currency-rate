package ru.alexeyzhulin.currencyrate.services;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ru.alexeyzhulin.currencyrate.exceptions.ApiException;
import ru.alexeyzhulin.currencyrate.models.ApiAnswer;

@Component
public abstract class RateGetter {
    private final String apiKey;


    protected RateGetter(Environment environment) {
        apiKey = environment.getProperty("application.apiKey");
    }

    abstract String getApiUrl();

    double getRateValue() throws UnirestException, ApiException {
        HttpResponse<JsonNode> response = Unirest.get(getApiUrl())
                .header("X-RapidAPI-Host", "fixer-fixer-currency-v1.p.rapidapi.com")
                .header("X-RapidAPI-Key", apiKey)
                .asJson();
        ApiAnswer apiAnswer = new Gson().fromJson(response.getBody().toString(), ApiAnswer.class);
        if (!apiAnswer.isSuccess()) {
            throw new ApiException(apiAnswer.getError().getCode() + " (" + apiAnswer.getError().getInfo() + ")");
        }
        return apiAnswer.getRates().getRUB();
    }
}
