package ru.alexeyzhulin.currencyrate.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Error info structure")
public class Error {
    @ApiModelProperty(notes = "Error code")
    private String code;
    @ApiModelProperty(notes = "Error type")
    private String type;
    @ApiModelProperty(notes = "Error info")
    private String info;

    public Error(String code, String type, String info) {
        this.code = code;
        this.type = type;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getInfo() {
        return info;
    }
}
