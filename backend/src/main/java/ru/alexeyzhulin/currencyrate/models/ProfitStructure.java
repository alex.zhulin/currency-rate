package ru.alexeyzhulin.currencyrate.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Profit structure")
public class ProfitStructure {
    @ApiModelProperty(notes = "Currency purchase rate")
    private double purchaseRate;
    @ApiModelProperty(notes = "Currency sell rate")
    private  double sellRate;
    @ApiModelProperty(notes = "Spread")
    private double spread;
    @ApiModelProperty(notes = "Currency amount")
    private double amount;
    @ApiModelProperty(notes = "Profit (positive value) or loss (negative value)")
    private double profit;

    public ProfitStructure(double purchaseRate, double sellRate, double spread, double amount, double profit) {
        this.purchaseRate = purchaseRate;
        this.sellRate = sellRate;
        this.spread = spread;
        this.amount = amount;
        this.profit = profit;
    }

    public double getPurchaseRate() {
        return purchaseRate;
    }

    public double getSellRate() {
        return sellRate;
    }

    public double getSpread() {
        return spread;
    }

    public double getAmount() {
        return amount;
    }

    public double getProfit() {
        return profit;
    }
}
