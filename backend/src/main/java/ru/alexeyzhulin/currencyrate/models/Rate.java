package ru.alexeyzhulin.currencyrate.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Currency rate structure")
public class Rate {
    @ApiModelProperty(notes = "Ruble to base currency (US dollar) rate")
    private double RUB;

    public Rate(double RUB) {
        this.RUB = RUB;
    }

    public double getRUB() {
        return RUB;
    }
}
