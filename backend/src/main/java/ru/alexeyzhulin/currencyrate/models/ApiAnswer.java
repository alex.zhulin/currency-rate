package ru.alexeyzhulin.currencyrate.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(description = "Answer structure from currency rate API")
public class ApiAnswer {
    @ApiModelProperty(notes = "Success result (y/n)")
    private boolean success;
    @ApiModelProperty(notes = "Rate date")
    private Date date;
    @ApiModelProperty(notes = "Currency rate")
    private Rate rates;
    @ApiModelProperty(notes = "Error info")
    private Error error;

    public ApiAnswer(boolean success, Date date, Rate rates, Error error) {
        this.success = success;
        this.date = date;
        this.rates = rates;
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public Date getDate() {
        return date;
    }

    public Rate getRates() {
        return rates;
    }

    public Error getError() {
        return error;
    }
}
