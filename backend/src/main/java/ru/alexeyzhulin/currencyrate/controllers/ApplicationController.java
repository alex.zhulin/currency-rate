package ru.alexeyzhulin.currencyrate.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.alexeyzhulin.currencyrate.models.ProfitStructure;
import ru.alexeyzhulin.currencyrate.services.RateService;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@RestController
@RequestMapping("/api/main")
@Api(value = "rest-api", description = "Main functions")
@CrossOrigin
public class ApplicationController {

    private static final Logger log = LoggerFactory.getLogger(ApplicationController.class);

    private final RateService rateService;

    public ApplicationController(RateService rateService) {
        this.rateService = rateService;
    }

    @ApiOperation(value = "Get currency sell profit / lost structure", response = ProfitStructure.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Successfully retrieved data"),
            @ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error"),
    }
    )
    @RequestMapping(value = "/get_profit_structure", produces = "application/json", method = RequestMethod.POST)
    public ProfitStructure getProfitStructure(HttpServletResponse response,
                                              String purchaseDate,
                                              double amount) {
        log.info("[getProfitStructure] Starting");
        try {
            ProfitStructure profitStructure = rateService.getProfitStructure(purchaseDate, amount);
            log.info("[getProfitStructure] Done");
            return profitStructure;
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            log.error("[getProfitStructure] Error [" + ExceptionUtils.getStackTrace(e) + "]");
        }
        return null;
    }
}
